<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
	
	<div class="sidebar-module">
		<h4>Archives</h4>
		<ol class="list-unstyled">
			<?php wp_get_archives('type=monthly'); ?>
		</ol>
	</div>
	<div class="sidebar-module">
		<h4>Suivez moi</h4>
		<ol class="list-unstyled">
			<li><a href="<?php echo get_option('twitter'); ?>">Twitter</a></li>
			<li><a href="<?php echo get_option('facebook'); ?>">Facebook</a></li>
		</ol>
	</div>
</div><!-- /.blog-sidebar -->

