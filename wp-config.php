<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'photographie' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'aurore' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'simploco' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#~SMZ$MrP]ys!bHLjnys5` :w_wjEb=sq8lm!~..WD`5Lk{xAu8{G10h77dy W^I' );
define( 'SECURE_AUTH_KEY',  'C8#CFLO?NLK*oPIA3L2O$fmlcg/?-}x}=pI=G8;.B*~[B?*IZFg`,hdk-`ydwrIP' );
define( 'LOGGED_IN_KEY',    ';6k!tcj5iroPoS6GHsq+FeoVC# k&H$.5LJ&P6q{$u,&GyO4_#5Q/3jLZ&q.8tm7' );
define( 'NONCE_KEY',        'Lc(e,M@Kf,;=N0NzpD%x+tB?c=!3 }?v#mA%JjMy58<$:q Bw[AF+P8plw!tTeVQ' );
define( 'AUTH_SALT',        '@[P~_Q^e0>*Y_@kmbyR!6QPlN!T7N2fp,6uA{6Q[-es`>3[8Vq#Plq2^Vx~^;vOE' );
define( 'SECURE_AUTH_SALT', '+W(@9tq]7@Gd<g?KVLy8oDGw(8R#[jb_mJch5#fD3ak)]VrP`D[Eurv]O`MJO26~' );
define( 'LOGGED_IN_SALT',   'BVtc)*lq;p&j5|U#L7 O8-FrI{LRCHc}c)NlN9cS`r|&Rv`!`|c;+3DT)D#it QW' );
define( 'NONCE_SALT',       ':~e?-xBmSrF4|s8CSBWrN.$_ft-drv}`?<T9*M+?rMh-+p=aTpdl*6/Wyu6y+ZU)' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
