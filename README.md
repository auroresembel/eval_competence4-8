Dans le cadre de l'évaluation de la compétence 4 et 8 j'ai décidé de travailler sur un projet personnel initié par mon conjoint. C'est un site d'articles sur le theme de l'histoire en générale. 
Les article peuvent faire partie de catégories différents: préhistoire, antiquité, moyen-age, ...
Aussi le client doit avoir la possibilité de rajouter, elever et/ou modifier des articles. 
Tous articles doit être présent sur une seul page avec une photo leur correspondant ansi qu'un résumé de l'article. Il est possible de voir voir l'intégralité de l'article en cliquant sur celui-ci. 
 


<!-- idetifiant admin -->
identifant: admin
mot de pass: PfFPtQxc@wte08@2zN

<!-- pour récupérer le projet  -->
-Créer un fichier dans lequel on placera la projet
-faire un git clone du projet (git clone git@gitlab.com:auroresembel/eval_competence4-8.git)

<!-- creer un vhost pour lacer un projet  -->
-Taper: sudo nano /etc/hosts 
-Ajouter une ligne 127.0.1.1  et_le_nom_du_projet.loc
-Ensuite taper: sudo nano /etc/apache2/sites-available/et_le_nom_du_projet.conf
-Copier/coller ceci en changeant les chemin d'accés et le nom du projet:
<VirtualHost *:80>
        ServerName eval_competence4-8
        DocumentRoot /home/chemin/chemin/et_le_nom_du_projet
        <Directory /home/chemin/chemin/et_le_nom_du_projet
                AllowOverride all
                Require all granted
        </Directory>
        ErrorLog /var/log/apache2/error.et_le_nom_du_projet.loc
        CustomLog /var/log/apache2/access.et_le_nom_du_projet.loc combined
</VirtualHost>
-Puis taper: sudo a2ensite et_le_nom_du_projet
-Enfin relacer le serveur apache en fesant: sudo systemctl reload apache2

Maintenant dans la bare URL taper: et_le_nom_du_projet.loc

<!-- Pour lancer le projet  -->
-Pour commencer il faut créer une basse de donnée au nom de votre choix
-Lancer le projet avec le v-host (avec les instructions ci-dessus)
-Ouvrir le projet avec l'éditeur de texte et modifier les coordonées de la BDD dans le fichier wp_config.php
-Faire un import du ficher passion_histoire.sql dans votre basse de donnée
-Et maintenant indiquer les identifiant et mot de passe (présents en haut de la page readme)